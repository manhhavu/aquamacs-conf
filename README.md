# My Aquamacs Configuration

This configuration is specific for my common tasks with Aquamacs, mainly text editing and Clojure development.

## How to use:
* Change to your Aquamacs preferences directory: ~/Library/Preferences/Aquamacs Emacs
* Clone
 
	$ git clone git://bitbucket.org/manhhavu/aquamacs-conf
	$ git submodule init

